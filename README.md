# Annotation worker

## Instalacja zależności

```
pip install -r requirements.txt
```

## Budowa Dockera

```
sudo docker build -t kuptoo/persister-queue-worker:1.0 .
```

## Zmienne środowiskowe 

* KUPTOO_RABBIT_HOST -  default: amqp://guest:guest@127.0.0.1:5672//
* KUPTOO_RABBIT_INSERT_QUEUE - default: kuptoo.insert
* KUPTOO_RABBIT_WRITE_QUEUE - default: kuptoo.persister
* KUPTOO_ELASTIC_HOST - default: 127.0.0.1
* KUPTOO_ELASTIC_PORT - default: 9200
* KUPTOO_ELASTIC_INDEX - default: corpus-allegro-01
* KUPTOO_WORKER_NAME - default: worker

# Uruchomienie

```
sudo docker run \
-e KUPTOO_RABBIT_HOST=amqp://test:test@192.168.1.101:5672// \
-e KUPTOO_ELASTIC_HOST=192.168.1.101 \
-e KUPTOO_REPORT_PROGRESS=1 \
kuptoo/persister-queue-worker:1.0
```