FROM python:3.8-slim-buster

RUN apt-get update
RUN apt-get install -y git

# set the working directory in the container
WORKDIR /code

RUN python3 -m venv /opt/venv

# Install dependencies:
COPY requirements.txt .
RUN . /opt/venv/bin/activate && pip install -r requirements.txt

# Run the application:
RUN mkdir persister/
COPY persister persister/
COPY run-persister-worker.py .
CMD . /opt/venv/bin/activate && exec python run-persister-worker.py
