from elasticsearch import helpers
from elasticsearch.client import IndicesClient


class ElasticResult:

    def __init__(self, result):
        self._result = result
        self._ids = None

    def __len__(self):
        return self._result['total']['value']

    def __contains__(self, item):
        if self._ids is None:
            self._ids = {rec["_id"]: None for rec in self}
        return item in self._ids

    def __iter__(self):
        return iter(self._result['hits'])

    def max_score(self):
        return self._result['total']['max_score']


class ElasticIndex:

    def __init__(self, es, index):
        self.index = index
        self.es = es
        self.indices = IndicesClient(self.es)

    def put_template(self, name, settings):
        return self.indices.put_template(name=name, body=settings, include_type_name=False)

    def del_template(self, name):
        return self.indices.delete_template(name=name)

    def get_template(self, name):
        return self.indices.get_template(name=name)

    def delete(self):
        return self.indices.delete(self.index)

    def create(self, mappings, force=False):

        if self.indices.exists(self.index):
            if not force:
                raise ValueError('Index {} exists...'.format(self.index))
            self.delete()

        return self.indices.create(self.index, mappings)

    def get_by_id(self, id):
        return self.es.get(index=self.index, id=id)

    def find(self, query, chunk=None):
        if not chunk:
            return self._search(query)
        return self._scroll(query, chunk)

    def _search(self, query):
        result = self.es.search(index=self.index, body=query)
        yield ElasticResult(result['hits'])

    def _scroll(self, query, chunk=1000, scroll_ttl='2m'):
        # Initialize the scroll
        page = self.es.search(
            index=self.index,
            scroll=scroll_ttl,
            size=chunk,
            body=query)

        scroll_size = len(page['hits']['hits'])

        sid = page['_scroll_id']

        # Start scrolling
        while scroll_size > 0:
            yield ElasticResult(page['hits'])

            sid = page['_scroll_id']
            page = self.es.scroll(scroll_id=sid, scroll=scroll_ttl)

            scroll_size = len(page['hits']['hits'])

    def exists(self, index, query):
        pass

    def bulk_insert(self, records, id_key):
        bulk = []
        for record in records:
            bulk.append({
                "_index": self.index,
                "_id": record[id_key],
                "_source": record
            })

        return helpers.bulk(self.es, bulk)
