from persister.elastic.indices.database_mapping import database_mapping

tag_list = {'size', 'measures', 'watts', 'price', 'bez', 'products', 'ip_type', 'surface', 'thickness', 'przed', 'speed',
     'resist', 'data_speed', 'fuel_consumption', 'ram', 'inches', 'pressure', 'przez', 'freqency', 'weight', 'dla',
     'current', 'light_type', 'voltage', 'group', 'connector_type', 'cpu_type', 'in', 'creation', 'w', 'ze', 'fraction',
     'energy_class', 'unknown', 'resolution', 'z', 'tire_size', 'cmd2', 'print_resolution', 'engine', 'noise', 'po',
     'room_size', 'na', 'za', 'number', 'obroty', 'pins', '3d_size', 'percentage', 'set', 'spojnik', 'pieces',
     'scale', 'sound', 'features_2', 'do', 'drive_type', 'lumen', 'capacity', 'ram_kind', 'name', 'horse_power',
     'od', 'nad', 'year', 'strength', 'brand', 'screw_type', 'time', 'przeciw', 'pod', 'calories', 'spf', 'range',
     'paper_size', 'nr', 'fi', 'serial', 'features_1', 'temperature', 'cable_type', 'field_size'}

available_tags = set(database_mapping['body']['mappings']['properties']['offer']['properties']['tags']['properties']['explicit']['properties'].keys())
print(available_tags)
print(available_tags.difference(tag_list))