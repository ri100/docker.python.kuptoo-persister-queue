import os

rabbit = {
    'host': os.environ[
        'KUPTOO_RABBIT_HOST'] if 'KUPTOO_RABBIT_HOST' in os.environ else 'amqp://guest:guest@127.0.0.1:5672//',
    # 'insert.queue': os.environ[
    #     'KUPTOO_RABBIT_INSERT_QUEUE'] if 'KUPTOO_RABBIT_INSERT_QUEUE' in os.environ else 'kuptoo.insert',
    'persist.queue': os.environ[
        'KUPTOO_RABBIT_WRITE_QUEUE'] if 'KUPTOO_RABBIT_WRITE_QUEUE' in os.environ else 'kuptoo.persister'
}

elastic = {
    'host': os.environ['KUPTOO_ELASTIC_HOST'] if 'KUPTOO_ELASTIC_HOST' in os.environ else '192.168.1.101',
    'port': os.environ['KUPTOO_ELASTIC_PORT'] if 'KUPTOO_ELASTIC_PORT' in os.environ else 9200,
    'index': os.environ['KUPTOO_ELASTIC_INDEX'] if 'KUPTOO_ELASTIC_INDEX' in os.environ else 'kuptoo-allegro-03'
}

worker = {
    'name': os.environ['KUPTOO_WORKER_NAME'] if 'KUPTOO_WORKER_NAME' in os.environ else 'worker',
    'report_progress_every': os.environ['KUPTOO_REPORT_PROGRESS'] if 'KUPTOO_REPORT_PROGRESS' in os.environ else '30',
}
