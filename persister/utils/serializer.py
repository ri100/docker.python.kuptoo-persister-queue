import pickle


def unserialize(file):
    with open(file, 'rb') as f:
        return pickle.load(f)
