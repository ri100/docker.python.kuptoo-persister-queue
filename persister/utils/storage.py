import elasticsearch
from elasticsearch import Elasticsearch, helpers


class Elastic:

    def __init__(self, host, index, port=9200):
        self._index = index
        self._cache = {}
        self._client = Elasticsearch([{'host': host, 'port': port}], sniff_on_start=True)

    def __contains__(self, item):
        try:
            if item in self._cache:
                return True
            self._cache[item] = self[item]
            return True
        except elasticsearch.exceptions.NotFoundError:
            self._cache = {}
            return False

    def __getitem__(self, item):
        if item in self._cache:
            return self._cache[item]
        self._cache[item] = self._client.get(index=self._index, id=item)
        return self._cache[item]

    def clear(self):
        self._cache = {}

    def insert(self, records):
        bulk = []
        for record in records:
            bulk.append({
                "_index": self._index,
                "_id": record['offer']['hash']['raw'],
                "_source": record
            })
        return helpers.bulk(self._client, bulk)


if __name__ == "__main__":
    ela = Elastic('127.0.0.1', 'kuptoo-allegro-01')
    print('215099ee651f05483289eee1f0d921d3' in ela)
    print(ela['215099ee651f05483289eee1f0d921d3'])
