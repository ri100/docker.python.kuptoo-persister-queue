import logging
import time

import elasticsearch
import pytz
from datetime import datetime, timezone

import urllib3

from persister.utils.storage import Elastic

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class ElasticHandler:

    def __init__(self, host, port, index, skip_existing=False, progress_report=0):
        self.host = host
        self.port = port
        self.index = index
        self.progress_report = progress_report * 60  # in minutes
        self.storage = self._connect()
        logger.info("Elasticsearch connected.")
        self.skip_existing = skip_existing
        self.time = time.time()
        self.indexed_items = 0

    def _connect(self):
        while True:
            logger.info("Connecting to Elasticsearch in 30 seconds.")
            time.sleep(30)
            return Elastic(self.host, self.index, port=self.port)

    def _exists(self, id):
        return id in self.storage

    def handle(self, payload_in):
        batch = []
        for item in payload_in:

            raw_hash = item['offer']['hash']['raw']

            if self.skip_existing and self._exists(raw_hash):
                continue

            today = datetime.now(timezone.utc)
            cet = pytz.timezone('Europe/Warsaw')

            item['meta'] = {
                'active': True,
                'inserted': today.astimezone(cet).strftime("%Y-%m-%d %H:%I:%S %Z")
            }

            batch.append(item)

        self.storage.insert(batch)
        self.indexed_items += len(batch)

        time_passed = time.time() - self.time
        if 0 < self.progress_report < time_passed:
            logger.info("Indexed {} records in {:06.4f} sec".format(self.indexed_items, time_passed))
            self.time = time.time()
            self.indexed_items = 0
