from elasticsearch import Elasticsearch
from persister.elastic.indices.database_mapping import database_mapping
from install.elastic.utils import ElasticIndex
from persister.config import elastic as config

es = Elasticsearch([{'host': config['host'], 'port': config['port']}])
idx = ElasticIndex(es, 'kuptoo')
idx.del_template('kuptoo-')
result = idx.put_template('kuptoo-', database_mapping['body'])
print(idx.get_template('kuptoo-'))
print(result)

available_tags = set(database_mapping['body']['mappings']['properties']['offer']['properties']['tag']['properties']['explicit']['properties'].keys())
