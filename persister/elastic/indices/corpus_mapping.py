from annotator_enums.enums import Key

corpus_mapping = {
    "index": "corpus",
    "body": {
        "index_patterns": [
            "corpus-*"
        ],
        "settings": {
            "number_of_shards": 5,
            "number_of_replicas": 1
        },
        "mappings": {
            "dynamic": "strict",
            "properties": {
                "_active": {
                    "type": "boolean",
                    "index": True
                },
                "_inserted": {
                    "type": "date",
                    "format": "yyyy-MM-dd HH:mm:ss z",
                    "index": True
                },
                "_updated": {
                    "type": "date",
                    "format": "yyyy-MM-dd HH:mm:ss z",
                    "index": True
                },
                "_deleted": {
                    "type": "date",
                    "format": "yyyy-MM-dd HH:mm:ss z",
                    "index": True
                },
                "offer": {
                    "properties": {
                        "title": {
                            "properties": {
                                "raw": {
                                    "type": "text",
                                    "index": False
                                },
                                "std": {
                                    "type": "text",
                                    "index": True
                                },
                                "std1": {
                                    "type": "text",
                                    "index": True
                                },
                                "std2": {
                                    "type": "text",
                                    "index": True
                                }
                            }
                        },
                        "description": {
                            "type": "text",
                            "index": True
                        },
                        "price": {
                            "type": "float",
                            "index": True
                        },
                        "photos": {
                            "properties": {
                                "local": {
                                    "properties": {
                                        "paths": {
                                            "type": "keyword",
                                            "index": False
                                        }
                                    }
                                },
                                "remote": {
                                    "properties": {
                                         "urls": {
                                            "type": "keyword",
                                            "index": False
                                        }
                                    }
                                }
                            }
                        },
                        "attr": {
                            "type": "nested",
                            "properties": {
                                "key": {
                                    "type": "keyword",
                                    "index": False
                                },
                                "name": {
                                    "type": "keyword",
                                    "index": False
                                },
                                "value": {
                                    "type": "keyword",
                                    "index": False
                                }
                            }
                        },
                        "producer": {
                            "properties": {
                                "name": {
                                    "type": "text",
                                    "index": True
                                },
                                "country": {
                                    "type": "keyword",
                                    "index": True
                                }
                            }
                        },
                        "supplier": {
                            "properties": {
                                "_id": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "name": {
                                    "type": "text",
                                    "index": True
                                },
                                "website": {
                                    "type": "keyword",
                                    "index": False
                                }
                            }
                        },
                        "_gid": {
                            "properties": {
                                "local": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "ean": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "isbn": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "partNo": {
                                    "type": "keyword",
                                    "index": True
                                }
                            }
                        },
                        "_category": {
                            "properties": {
                                "id": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "idx": {
                                    "type": "integer",
                                    "index": True
                                },
                                "cidx": {
                                    "type": "integer",
                                    "index": True
                                }
                            }
                        },
                        "_tags": {
                            "properties": {
                                "version": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "auto": {
                                    "properties": {
                                        Key.PRODUKT1: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 2
                                        },
                                        Key.WYTWORY: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.MARKETING: {
                                            "type": "keyword",
                                            "index": False,
                                            "boost": 0.1
                                        },
                                        Key.MARKA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.5
                                        },
                                        Key.ADJ: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.3
                                        },
                                        Key.REL_DOP: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.5
                                        },
                                        Key.REL_ANTY: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_BEZ: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_DLA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_DO: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_NA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_NAD: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_OD: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_O: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_POD: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PO: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PRZECIW: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PRZED: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PRZEZ: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_W: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_Z: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_ZA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_ZE: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.EAN: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 4
                                        },
                                        Key.SERIAL: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 4
                                        },
                                        Key.PART_NO: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 4
                                        },
                                        Key.WERSJA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.PRAD: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.NAPIECIE: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.SZTUK: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.DZWIEK: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.POJEMNOSC: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.CZESCTOTLIWOSC: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.CZAS: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.ROZMIAR_OPON: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.5
                                        },
                                        Key.PREDKOSC_DANYCH: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.PAMIEC: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.OPORNOSC: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.FOKUS: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.CALE: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.ROZDZIELCZOSC: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.MATRYCA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.RODZAJ_PAMIECI: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.ILOSC_PINOW: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.CENA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.RODZAJ_GWINTU: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.GRUBOSC: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.ROZMIAR: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.CISNIENIE: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.WAGA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.ROZMIAR_PAPIERU: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.ZLACZE: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.NAPED: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.LUMENY: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.SPF: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.NR: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.W_JEDNYM: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.SKALA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.PRZEDZIAL: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.ROCZNIK: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.ULAMEK: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.POWIERZCHNIA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.RODZIAJ_PROCESORA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.GRUPA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.RODZIAJ_PLYTY: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.BARWA_SWIATLA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.RODZAJ_KABLA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.KLASA_ENERG: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.IP_STOPIEN_OCHRONY: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.CDM2: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.NIEZNANE: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        }
                                    }
                                },
                                "super": {
                                    "properties": {
                                        Key.PRODUKT1: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 2
                                        },
                                        Key.WYTWORY: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.MARKETING: {
                                            "type": "keyword",
                                            "index": False,
                                            "boost": 0.1
                                        },
                                        Key.MARKA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.5
                                        },
                                        Key.ADJ: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.3
                                        },
                                        Key.REL_DOP: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.5
                                        },
                                        Key.REL_ANTY: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_BEZ: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_DLA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_DO: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_NA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_NAD: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_OD: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_O: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_POD: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PO: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PRZECIW: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PRZED: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PRZEZ: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_W: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_Z: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_ZA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_ZE: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.EAN: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 4
                                        },
                                        Key.SERIAL: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 4
                                        },
                                        Key.PART_NO: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 4
                                        },
                                        Key.WERSJA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.PRAD: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.NAPIECIE: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.SZTUK: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.DZWIEK: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.POJEMNOSC: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.CZESCTOTLIWOSC: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.CZAS: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.ROZMIAR_OPON: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.5
                                        },
                                        Key.PREDKOSC_DANYCH: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.PAMIEC: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.OPORNOSC: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.FOKUS: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.CALE: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.ROZDZIELCZOSC: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.MATRYCA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.RODZAJ_PAMIECI: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.ILOSC_PINOW: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.CENA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.RODZAJ_GWINTU: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.GRUBOSC: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.ROZMIAR: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.CISNIENIE: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.WAGA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.ROZMIAR_PAPIERU: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.ZLACZE: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.NAPED: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.LUMENY: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.SPF: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.NR: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.W_JEDNYM: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.SKALA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.PRZEDZIAL: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.ROCZNIK: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.ULAMEK: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.POWIERZCHNIA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.RODZIAJ_PROCESORA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.GRUPA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.RODZIAJ_PLYTY: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.BARWA_SWIATLA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.RODZAJ_KABLA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.KLASA_ENERG: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.IP_STOPIEN_OCHRONY: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.CDM2: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        Key.NIEZNANE: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        }
                                    }
                                },
                            }
                        },
                        "_hash": {
                            "properties": {
                                "raw": {
                                    "type": "keyword",
                                    "index": True,
                                    "ignore_above": 32
                                },
                                "std": {
                                    "type": "keyword",
                                    "index": True,
                                    "ignore_above": 32
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
