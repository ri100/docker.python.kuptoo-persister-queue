from annotator_enums.enums import Key

database_mapping = {
    "index": "kuptoo",
    "body": {
        "index_patterns": [
            "kuptoo-*"
        ],
        "settings": {
            "number_of_shards": 5,
            "number_of_replicas": 1
        },
        "mappings": {
            "dynamic": "strict",
            "properties": {
                "meta": {
                    "properties": {
                        "active": {
                            "type": "boolean",
                            "index": True
                        },
                        "inserted": {
                            "type": "date",
                            "format": "yyyy-MM-dd HH:mm:ss z",
                            "index": True
                        },
                        "updated": {
                            "type": "date",
                            "format": "yyyy-MM-dd HH:mm:ss z",
                            "index": True
                        },
                        "deleted": {
                            "type": "date",
                            "format": "yyyy-MM-dd HH:mm:ss z",
                            "index": True
                        },
                    },
                },
                "offer": {
                    "properties": {
                        "title": {
                            "properties": {
                                "raw": {
                                    "type": "text",
                                    "index": False
                                },
                                "std": {
                                    "type": "text",
                                    "index": True
                                },
                            }
                        },
                        "url": {
                            "type": "keyword",
                            "index": False
                        },
                        "description": {
                            "type": "text",
                            "index": True
                        },
                        "price": {
                            "properties": {
                                "gross": {
                                    "properties": {
                                        "amount": {
                                            "type": "float",
                                            "index": True
                                        },
                                        "currency": {
                                            "type": "keyword",
                                            "index": True
                                        },
                                        "prev": {
                                            "properties": {
                                                "amount": {
                                                    "type": "float",
                                                    "index": False
                                                },
                                                "currency": {
                                                    "type": "keyword",
                                                    "index": False
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        "photos": {
                            "properties": {
                                "local": {
                                    "properties": {
                                        "default": {
                                            "type": "integer",
                                            "index": False
                                        },
                                        "paths": {
                                            "type": "keyword",
                                            "index": False
                                        }
                                    }
                                },
                                "remote": {
                                    "properties": {
                                        "default": {
                                            "type": "integer",
                                            "index": False
                                        },
                                        "urls": {
                                            "type": "keyword",
                                            "index": False
                                        }
                                    }
                                }
                            }
                        },
                        "availability": {
                            "properties": {
                                "stock": {
                                    "type": "integer",
                                    "index": True
                                },
                                "remote": {
                                    "type": "boolean",
                                    "index": True
                                }
                            }
                        },
                        "attr": {
                            "type": "nested",
                            "properties": {
                                "key": {
                                    "type": "keyword",
                                    "index": False
                                },
                                "name": {
                                    "type": "keyword",
                                    "index": False
                                },
                                "value": {
                                    "type": "keyword",
                                    "index": False
                                }
                            }
                        },
                        "producer": {
                            "properties": {
                                "name": {
                                    "type": "text",
                                    "index": True
                                },
                                "country": {
                                    "type": "keyword",
                                    "index": True
                                }
                            }
                        },
                        "supplier": {
                            "properties": {
                                "id": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "name": {
                                    "type": "text",
                                    "index": True
                                },
                                "website": {
                                    "type": "keyword",
                                    "index": False
                                }
                            }
                        },
                        "gid": {
                            "properties": {
                                "local": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "remote": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "ean": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "isbn": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "redisKey": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "partNo": {
                                    "type": "keyword",
                                    "index": True
                                }
                            }
                        },
                        "context": {
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "index": True
                                },
                                "name": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "probability": {
                                    "type": "float",
                                    "index": True
                                },
                            }
                        },
                        "category": {
                            "properties": {
                                "id": {
                                    "type": "integer",
                                    "index": True
                                },
                                "name": {
                                    "type": "keyword",
                                    "index": True
                                },
                                "probability": {
                                    "type": "float",
                                    "index": True
                                },
                                "valid": {
                                    "type": "boolean",
                                    "index": True
                                },
                                "vector": {
                                    "type": "dense_vector",
                                    "dims": 300
                                }
                            }
                        },
                        "labels": {
                            "properties": {
                                "context": {
                                    "properties": {
                                        "id": {
                                            "type": "integer",
                                            "index": True
                                        },
                                        "name": {
                                            "type": "keyword",
                                            "index": True
                                        },
                                        "probability": {
                                            "type": "float",
                                            "index": True
                                        }
                                    }
                                },
                                "products": {
                                    "properties": {
                                        "id": {
                                            "type": "integer",
                                            "index": True
                                        },
                                        "name": {
                                            "type": "keyword",
                                            "index": True
                                        },
                                        "probability": {
                                            "type": "float",
                                            "index": True
                                        }
                                    }
                                },
                                "action": {
                                    "properties": {
                                        "id": {
                                            "type": "integer",
                                            "index": True
                                        },
                                        "name": {
                                            "type": "keyword",
                                            "index": True
                                        },
                                        "probability": {
                                            "type": "float",
                                            "index": True
                                        }
                                    }
                                },
                                "rel": {
                                    "properties": {
                                        "id": {
                                            "type": "integer",
                                            "index": True
                                        },
                                        "name": {
                                            "type": "keyword",
                                            "index": True
                                        },
                                        "probability": {
                                            "type": "float",
                                            "index": True
                                        }
                                    }
                                }
                            }
                        },
                        "tag": {
                            "properties": {
                                "explicit": {
                                    "properties": {
                                        "value": {
                                            "properties": {
                                                'products': {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 2
                                                },
                                                Key.WYTWORY: {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 1
                                                },
                                                Key.MARKETING: {
                                                    "type": "keyword",
                                                    "index": False,
                                                    "boost": 0.1
                                                },
                                                Key.MARKA: {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 1.5
                                                },
                                                'features_1': {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 1.3
                                                },
                                                'features_2': {
                                                    "type": "text",
                                                    "index": True,
                                                    "boost": 1.5
                                                },
                                                Key.REL_BEZ: {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 1.2
                                                },
                                                Key.REL_DLA: {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 1.2
                                                },
                                                Key.REL_DO: {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 1.2
                                                },
                                                Key.REL_NA: {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 1.2
                                                },
                                                Key.REL_NAD: {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 1.2
                                                },
                                                Key.REL_OD: {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 1.2
                                                },
                                                Key.REL_POD: {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 1.2
                                                },
                                                Key.REL_PO: {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 1.2
                                                },
                                                Key.REL_PRZECIW: {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 1.2
                                                },
                                                Key.REL_PRZED: {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 1.2
                                                },
                                                Key.REL_PRZEZ: {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 1.2
                                                },
                                                Key.REL_W: {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 1.2
                                                },
                                                Key.REL_Z: {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 1.2
                                                },
                                                Key.REL_ZA: {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 1.2
                                                },
                                                Key.REL_ZE: {
                                                    "type": "keyword",
                                                    "index": True,
                                                    "boost": 1.2
                                                },
                                                "size": {"type": "keyword", "index": True, "boost": 1},
                                                "measures": {"type": "keyword", "index": True, "boost": 1},
                                                "watts": {"type": "keyword", "index": True, "boost": 1},
                                                "price": {"type": "keyword", "index": True, "boost": 1},
                                                "ip_type": {"type": "keyword", "index": True, "boost": 1},
                                                "surface": {"type": "keyword", "index": True, "boost": 1},
                                                "thickness": {"type": "keyword", "index": True, "boost": 1},
                                                "speed": {"type": "keyword", "index": True, "boost": 1},
                                                "resist": {"type": "keyword", "index": True, "boost": 1},
                                                "data_speed": {"type": "keyword", "index": True, "boost": 1},
                                                "fuel_consumption": {"type": "keyword", "index": True, "boost": 1},
                                                "ram": {"type": "keyword", "index": True, "boost": 1},
                                                "inches": {"type": "keyword", "index": True, "boost": 1},
                                                "pressure": {"type": "keyword", "index": True, "boost": 1},
                                                "przez": {"type": "keyword", "index": True, "boost": 1},
                                                "freqency": {"type": "keyword", "index": True, "boost": 1},
                                                "weight": {"type": "keyword", "index": True, "boost": 1},
                                                "current": {"type": "keyword", "index": True, "boost": 1},
                                                "light_type": {"type": "keyword", "index": True, "boost": 1},
                                                "voltage": {"type": "keyword", "index": True, "boost": 1},
                                                "group": {"type": "keyword", "index": True, "boost": 1},
                                                "connector_type": {"type": "keyword", "index": True, "boost": 1},
                                                "cpu_type": {"type": "keyword", "index": True, "boost": 1},
                                                "in": {"type": "keyword", "index": True, "boost": 1},
                                                "creation": {"type": "keyword", "index": True, "boost": 1},
                                                "fraction": {"type": "keyword", "index": True, "boost": 1},
                                                "energy_class": {"type": "keyword", "index": True, "boost": 1},
                                                "resolution": {"type": "keyword", "index": True, "boost": 1},
                                                "tire_size": {"type": "keyword", "index": True, "boost": 1},
                                                "cmd2": {"type": "keyword", "index": True, "boost": 1},
                                                "print_resolution": {"type": "keyword", "index": True, "boost": 1},
                                                "engine": {"type": "keyword", "index": True, "boost": 1},
                                                "noise": {"type": "keyword", "index": True, "boost": 1},
                                                "room_size": {"type": "keyword", "index": True, "boost": 1},
                                                "number": {"type": "keyword", "index": False, "boost": 1},
                                                "obroty": {"type": "keyword", "index": True, "boost": 1},
                                                "pins": {"type": "keyword", "index": True, "boost": 1},
                                                "3d_size": {"type": "keyword", "index": True, "boost": 1},
                                                "percentage": {"type": "keyword", "index": True, "boost": 1},
                                                "set": {"type": "keyword", "index": True, "boost": 1},
                                                "spojnik": {"type": "keyword", "index": True, "boost": 1},
                                                "pieces": {"type": "keyword", "index": True, "boost": 1},
                                                "scale": {"type": "keyword", "index": True, "boost": 1},
                                                "sound": {"type": "keyword", "index": True, "boost": 1},
                                                "drive_type": {"type": "keyword", "index": True, "boost": 1},
                                                "lumen": {"type": "keyword", "index": True, "boost": 1},
                                                "capacity": {"type": "keyword", "index": True, "boost": 1},
                                                "ram_kind": {"type": "keyword", "index": True, "boost": 1},
                                                "name": {"type": "keyword", "index": True, "boost": 1},
                                                "horse_power": {"type": "keyword", "index": True, "boost": 1},
                                                "year": {"type": "keyword", "index": True, "boost": 1},
                                                "strength": {"type": "keyword", "index": True, "boost": 1},
                                                "brand": {"type": "keyword", "index": True, "boost": 1},
                                                "screw_type": {"type": "keyword", "index": True, "boost": 1},
                                                "time": {"type": "keyword", "index": True, "boost": 1},
                                                "przeciw": {"type": "keyword", "index": True, "boost": 1},
                                                "calories": {"type": "keyword", "index": True, "boost": 1},
                                                "spf": {"type": "keyword", "index": True, "boost": 1},
                                                "range": {"type": "keyword", "index": True, "boost": 1},
                                                "paper_size": {"type": "keyword", "index": True, "boost": 1},
                                                "nr": {"type": "keyword", "index": True, "boost": 1},
                                                "fi": {"type": "keyword", "index": True, "boost": 1},
                                                "serial": {"type": "keyword", "index": True, "boost": 1},
                                                "temperature": {"type": "keyword", "index": True, "boost": 1},
                                                "cable_type": {"type": "keyword", "index": True, "boost": 1},
                                                "field_size": {"type": "keyword", "index": True, "boost": 1},
                                            }
                                        },
                                        "probability": {
                                            "properties": {
                                                'products': {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                Key.WYTWORY: {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                Key.MARKETING: {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                Key.MARKA: {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                'features_1': {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                'features_2': {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                Key.REL_BEZ: {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                Key.REL_DLA: {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                Key.REL_DO: {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                Key.REL_NA: {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                Key.REL_NAD: {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                Key.REL_OD: {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                Key.REL_POD: {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                Key.REL_PO: {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                Key.REL_PRZECIW: {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                Key.REL_PRZED: {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                Key.REL_PRZEZ: {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                Key.REL_W: {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                Key.REL_Z: {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                Key.REL_ZA: {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                Key.REL_ZE: {
                                                    "type": "float",
                                                    "index": False,
                                                },
                                                "size": {"type": "float", "index": False},
                                                "measures": {"type": "float", "index": False},
                                                "watts": {"type": "float", "index": False},
                                                "price": {"type": "float", "index": False},
                                                "ip_type": {"type": "float", "index": False},
                                                "surface": {"type": "float", "index": False},
                                                "thickness": {"type": "float", "index": False},
                                                "speed": {"type": "float", "index": False},
                                                "resist": {"type": "float", "index": False},
                                                "data_speed": {"type": "float", "index": False},
                                                "fuel_consumption": {"type": "float", "index": False},
                                                "ram": {"type": "float", "index": False},
                                                "inches": {"type": "float", "index": False},
                                                "pressure": {"type": "float", "index": False},
                                                "przez": {"type": "float", "index": False},
                                                "freqency": {"type": "float", "index": False},
                                                "weight": {"type": "float", "index": False},
                                                "current": {"type": "float", "index": False},
                                                "light_type": {"type": "float", "index": False},
                                                "voltage": {"type": "float", "index": False},
                                                "group": {"type": "float", "index": False},
                                                "connector_type": {"type": "float", "index": False},
                                                "cpu_type": {"type": "float", "index": False},
                                                "in": {"type": "float", "index": False},
                                                "creation": {"type": "float", "index": False},
                                                "fraction": {"type": "float", "index": False},
                                                "energy_class": {"type": "float", "index": False},
                                                "resolution": {"type": "float", "index": False},
                                                "tire_size": {"type": "float", "index": False},
                                                "cmd2": {"type": "float", "index": False},
                                                "print_resolution": {"type": "float", "index": False},
                                                "engine": {"type": "float", "index": False},
                                                "noise": {"type": "float", "index": False},
                                                "room_size": {"type": "float", "index": False},
                                                "number": {"type": "float", "index": False},
                                                "obroty": {"type": "float", "index": False},
                                                "pins": {"type": "float", "index": False},
                                                "3d_size": {"type": "float", "index": False},
                                                "percentage": {"type": "float", "index": False},
                                                "set": {"type": "float", "index": False},
                                                "spojnik": {"type": "float", "index": False},
                                                "pieces": {"type": "float", "index": False},
                                                "scale": {"type": "float", "index": False},
                                                "sound": {"type": "float", "index": False},
                                                "drive_type": {"type": "float", "index": False},
                                                "lumen": {"type": "float", "index": False},
                                                "capacity": {"type": "float", "index": False},
                                                "ram_kind": {"type": "float", "index": False},
                                                "name": {"type": "float", "index": False},
                                                "horse_power": {"type": "float", "index": False},
                                                "year": {"type": "float", "index": False},
                                                "strength": {"type": "float", "index": False},
                                                "brand": {"type": "float", "index": False},
                                                "screw_type": {"type": "float", "index": False},
                                                "time": {"type": "float", "index": False},
                                                "przeciw": {"type": "float", "index": False},
                                                "calories": {"type": "float", "index": False},
                                                "spf": {"type": "float", "index": False},
                                                "range": {"type": "float", "index": False},
                                                "paper_size": {"type": "float", "index": False},
                                                "nr": {"type": "float", "index": False},
                                                "fi": {"type": "float", "index": False},
                                                "serial": {"type": "float", "index": False},
                                                "temperature": {"type": "float", "index": False},
                                                "cable_type": {"type": "float", "index": False},
                                                "field_size": {"type": "float", "index": False},
                                            }
                                        }
                                    }
                                },
                                "stem": {
                                    "properties": {
                                        'products': {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 2
                                        },
                                        Key.WYTWORY: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1
                                        },
                                        'features_1': {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.3
                                        },
                                        'features_2': {
                                            "type": "text",
                                            "index": True,
                                            "boost": 1.5
                                        },
                                        Key.REL_BEZ: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_DLA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_DO: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_NA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_NAD: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_OD: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_POD: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PO: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PRZECIW: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PRZED: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_PRZEZ: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_W: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_Z: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_ZA: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        },
                                        Key.REL_ZE: {
                                            "type": "keyword",
                                            "index": True,
                                            "boost": 1.2
                                        }
                                    }
                                },
                            }
                        },
                        "hash": {
                            "properties": {
                                "raw": {
                                    "type": "keyword",
                                    "index": True,
                                    "ignore_above": 32
                                },
                                "std": {
                                    "type": "keyword",
                                    "index": True,
                                    "ignore_above": 32
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
