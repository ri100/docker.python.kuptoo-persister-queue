import logging
from time import sleep
from kombu import Connection
from kuptoo_queue.worker.queue2handler import Queue2Handler
from kuptoo_queue.worker.queue_meta import QueueMeta

from persister import config
from persister.handler.persister import ElasticHandler

logging.basicConfig(format='%(asctime)s %(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

elastic_host = config.elastic['host']
elastic_port = config.elastic['port']
elastic_index = config.elastic['index']
elastic_index_progress = float(config.worker['report_progress_every']) if config.worker[
    'report_progress_every'].isnumeric() else 0

rabbit_host = config.rabbit['host']
rabbit_in_queue = config.rabbit['persist.queue']

logger.info("Elastic at {}".format(elastic_host))
logger.info("Indexing to {}".format(elastic_index))
logger.info("Indexing will be reported every {} minutes".format(elastic_index_progress))

while True:
    try:
        logger.info("Connecting to {}".format(rabbit_host))
        with Connection(rabbit_host, connect_timeout=20) as conn:
            handler = ElasticHandler(elastic_host, elastic_port, elastic_index, progress_report=elastic_index_progress)
            in_queue = QueueMeta(rabbit_in_queue, routing_key='offer.TAG')
            worker = Queue2Handler(handler, conn, in_queue, worker_name_prefix='persister-')

            worker.run()
    except Exception as e:
        sleep(1)
